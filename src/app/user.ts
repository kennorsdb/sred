export class User {
    _id: String;
    nombre: String;
    apellidos: String;
    userName: String;
    pass: String;
    email: String;
    sede: String;
    puesto: String;
    rol: [String];
}
