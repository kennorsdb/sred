import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { ProyectoComponent } from './proyecto/proyecto.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { DebilidadesCausasComponent } from './debilidades-causas/debilidades-causas.component';
import { ObjIndicadoresComponent, ObjetivoDialogComponent  } from './obj-indicadores/obj-indicadores.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ActividadComponent } from './actividad/actividad.component';
import { SedeComponent } from './sede/sede.component';
import { GraficosComponent } from './graficos/graficos.component';
import { InfoEscuelaComponent } from './info-escuela/info-escuela.component';
import { EjecucionActividadesComponent } from './ejecucion-actividades/ejecucion-actividades.component';
import { EvidenciasComponent, AgregarEvidenciaDialogComponent  } from './evidencias/evidencias.component';


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'usuario', component: UsuarioComponent},
  {path: 'registrarUsuario', component: SignUpComponent},
  {path: 'proyecto', component: ProyectoComponent},
  {path: 'debilidadcausa', component: DebilidadesCausasComponent},
  {path: 'objetivosIndicadores', component: ObjIndicadoresComponent},
  {path: 'perfil', component: PerfilComponent},
  {path: 'app-objetivo-edit-dialog', component: ObjetivoDialogComponent},
  {path: 'actividades', component: ActividadComponent},
  {path: 'sede', component: SedeComponent},
  {path: 'graficos', component: GraficosComponent},
  {path: 'infoEscuela', component: InfoEscuelaComponent},
  {path: 'ejecucionActividades', component: EjecucionActividadesComponent},
  {path: 'agregarEvidencias', component: EvidenciasComponent},
  {path: 'app-agregar-evidencia', component: AgregarEvidenciaDialogComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
