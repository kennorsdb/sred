import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { ProyectoComponent } from './proyecto/proyecto.component';

import { MatButtonModule, MatCheckboxModule} from '@angular/material';
import { MatTableModule, MatPaginatorModule} from '@angular/material';
import { MatFormFieldModule, MatInputModule} from '@angular/material';
import { MatSortModule, MatListModule} from '@angular/material';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSelectModule} from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule  } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSliderModule } from '@angular/material/slider';


import { UsuarioListComponent } from './usuario-list/usuario-list.component';
import { UsuarioDetalleComponent } from './usuario-detalle/usuario-detalle.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { DebilidadesCausasComponent } from './debilidades-causas/debilidades-causas.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ObjIndicadoresComponent, ObjetivoDialogComponent } from './obj-indicadores/obj-indicadores.component';
import { ActividadComponent } from './actividad/actividad.component';
import { SedeComponent } from './sede/sede.component';
import { GraficosComponent } from './graficos/graficos.component';

import { InfoEscuelaComponent } from './info-escuela/info-escuela.component';
import { EjecucionActividadesComponent } from './ejecucion-actividades/ejecucion-actividades.component';
import { EvidenciasComponent, AgregarEvidenciaDialogComponent } from './evidencias/evidencias.component';
import { FileSelectDirective } from 'ng2-file-upload';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignUpComponent,
    ProyectoComponent,
    UsuarioListComponent,
    UsuarioDetalleComponent,
    UsuarioComponent,
    DebilidadesCausasComponent,
    PerfilComponent,
    ObjIndicadoresComponent,
    ActividadComponent,
    ObjetivoDialogComponent,
    SedeComponent,
    GraficosComponent,
    InfoEscuelaComponent,
    EjecucionActividadesComponent,
    EvidenciasComponent,
    AgregarEvidenciaDialogComponent,
    FileSelectDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatSortModule,
    MatListModule,
    MatSelectModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    ChartsModule,
    MatExpansionModule,
    MatSliderModule


  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
