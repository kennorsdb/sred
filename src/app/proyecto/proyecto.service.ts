import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Proyecto } from './proyecto';

@Injectable()
export class ProyectoService {

    private _getUrl = 'api/manualSinaes';
    private _postUrl = 'api/manualSinaes';
    private _putUrl = 'api/manualSinaes';
    private _deleteUrl = 'api/manualSinaes';

    constructor(private _http: Http) { }

    getManualSinaes() {
        return this._http.get(this._getUrl)
            .map((response: Response) => response.json());
    }

    getDimensiones() {
        return this._http.get('api/dimensiones')
            .map((response: Response) => response.json());
    }
}
