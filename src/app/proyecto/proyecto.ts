export class Proyecto {
    _id: String;
    dimension: String;
    componente: String;
    criterio: String;
    descripcion: String;
    estandares: Array<Estandares>;
    evidenciasEsperadas: Array<EvidenciasEsperadas>;
}

export class Estandares {
    numero: String;
    descripcion: String;
}

export class EvidenciasEsperadas {
    numero: String;
    descripcion: String;
}