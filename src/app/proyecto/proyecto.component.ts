import { Component, OnInit, ViewChild } from '@angular/core';
import { ProyectoService } from './proyecto.service';
import { Proyecto } from './proyecto';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.css'],
  providers: [ProyectoService]
})


export class ProyectoComponent implements OnInit {
  displayedColumns = ['dimension', 'descripcion'];
  manualSinaes: any;
  dataSource: any;
  dimensiones: any;
  dimension: any;
  componente: any;

  constructor(private _ProyectoService: ProyectoService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this._ProyectoService.getManualSinaes()
      .subscribe(manualData => {
        this.manualSinaes = manualData;
        this.dataSource = new MatTableDataSource(this.manualSinaes);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dimension = manualData[0].Ndimension;
      });

    this._ProyectoService.getDimensiones()
    .subscribe(dimensionesData => {
      this.dimensiones = dimensionesData.dimensiones;
    });
  }

  applyDimension() {
    this.componente = this.dimension.componentes[0];
    this.dataSource.filterPredicate = (data, filter) => {
      if (data.dimension == this.dimension.numero &&
        data.componente == this.componente.numero) {
          return true;
      }
    };
    this.dataSource.filter = "a";
  }
  applyComponente() {
    this.dataSource.filterPredicate = (data, filter) => {
      if (data.dimension == this.dimension.numero &&
        data.componente == this.componente.numero) {
          return true;
      }
    };
    this.dataSource.filter = "a";
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    console.log(this.componente.numero);
    this.dataSource.filterPredicate = (data, filter) => {
      if (data.dimension == this.dimension.numero &&
        data.componente == this.componente.numero) {
          return true;
      }
    };
    this.dataSource.filter = filterValue;
  }




}
