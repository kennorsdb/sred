import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario/usuario.service';
import { User} from '../user';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  providers: [UsuarioService]
})
export class PerfilComponent implements OnInit {
	usuario: any;

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.usuarioService.getUsers()
      .subscribe(resUsuarioData => this.usuario = resUsuarioData);
  }

  onUpdateUsuarioEvent(usuario: User) {
    this.usuarioService.updateUsuario(usuario)
    .subscribe(resUsuarioData => this.usuario = resUsuarioData);
  }


}
