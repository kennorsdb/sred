import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../user';
import { ParametrosService } from '../parametrosService';
@Component({
  selector: 'usuario-detalle',
  templateUrl: './usuario-detalle.component.html',
  styleUrls: ['./usuario-detalle.component.css'],
  providers: [ParametrosService]
})
export class UsuarioDetalleComponent implements OnInit {

  @Input()
  usuario: any;

  rol: any;

  sedes: any;

  @Output()
  private updateUsuarioEvent = new EventEmitter();

  @Output()
  private deleteUsuarioEvent = new EventEmitter();

  constructor(private parametrosService: ParametrosService) { }

  ngOnInit() {
    console.log(this.usuario);
    this.parametrosService.getSedes()
      .subscribe(sedesData => {this.sedes = sedesData; console.log(this.sedes); });
  }

  updateUsuario() {
    this.updateUsuarioEvent.emit(this.usuario);
  }

  deleteUsuario() {
    this.deleteUsuarioEvent.emit(this.usuario);
  }

}
