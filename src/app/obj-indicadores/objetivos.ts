import { Compromiso } from '../debilidades-causas/compromiso';
import { Actividad } from '../actividad/actividad';

export class Objetivo {
    _id: String;
    compromisoMejora: Compromiso;
    objetivo: String;
    indicadores: Array<Indicador>;
}

class Indicador {
    numero: number;
    descripcion: String;
    actividades: Array<Actividad>;
}
