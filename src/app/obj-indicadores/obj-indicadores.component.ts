import { Component, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ParametrosService } from '../parametrosService';
import { CompromisoService } from '../debilidades-causas/compromiso.service';
import { ProyectoService } from '../proyecto/proyecto.service';
import { ObjetivoService } from './objetivos.service';
import { Compromiso } from '../debilidades-causas/compromiso';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
   selector: 'app-obj-indicadores',
   templateUrl: './obj-indicadores.component.html',
   styleUrls: ['./obj-indicadores.component.css'],
   providers: [ProyectoService, ParametrosService, CompromisoService, ObjetivoService]
})
export class ObjIndicadoresComponent implements OnInit {
   displayedColumns = ['No.', 'Debilidad', 'Causa', 'Objetivo', 'Indicadores'];
   manualSinaes: any;
   dataSource: any;

   dimensiones: any;
   dimension: any;
   componente: any;
   criterios: any;
   criterio: any;
   debilidades: any;
   sedes: any;
   sede: any;
   debilidad: any;
   causa: any;
   objetivos: any;
   objetivo: any;

   compromiso: any;
   constructor(private _ProyectoService: ProyectoService,
      private _ParametrosService: ParametrosService,
      private _CompromisoService: CompromisoService,
      private _ObjetivoService: ObjetivoService,
      public dialog: MatDialog) { }

   ngOnInit() {
      this._ProyectoService.getManualSinaes()
         .subscribe(manualData => {
            this.manualSinaes = manualData;
            this.dimension = manualData[0].Ndimension;
         });

      this._ProyectoService.getDimensiones()
         .subscribe(dimensionesData => {
            this.dimensiones = dimensionesData.dimensiones;
         });

      this._ParametrosService.getSedes()
         .subscribe(sedesData => {
            this.sedes = sedesData;
         });

      this._CompromisoService.getCompromisos()
         .subscribe(compromisoData => {
            this.debilidades = compromisoData;
            // Objetivos
            this._ObjetivoService.getobjetivos().subscribe(objetivosData => {
               this.objetivos = objetivosData;
               // tslint:disable-next-line:forin
               for (let i in this.debilidades) {
                  let d = this.debilidades[i];
                  let unica = true;
                  for (let j in this.objetivos) {
                     if (d._id === this.objetivos[j].compromisoMejora ||
                        d._id === this.objetivos[j].compromisoMejora._id) {
                        unica = false;
                        this.objetivos[j].compromisoMejora = d;
                     }
                  }
                  if (unica) {
                     this.objetivos.push({
                        compromisoMejora: d
                     });
                  }
               }
               this.dataSource = new MatTableDataSource(this.objetivos);
               console.log(this.objetivos);
            });
         });
   }

   applyDimension() {
      this.componente = this.dimension.componentes[0];
   }

   applyComponente() {
      let m = this.manualSinaes;
      this.criterios = [];
      for (let i in m) {
         if (m[i].dimension == this.dimension.numero &&
            m[i].componente == this.componente.numero) {
            this.criterios.push(m[i]);
         }
      }
   }

   applyCriterio() {
      this.applyFilter('a');
   }


   applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      
      this.dataSource.filterPredicate = (data, filter) => {
         console.log(data);
         console.log(this.criterio);
         const d = data.compromisoMejora.manualSinaes;
         if (d._id == this.criterio._id) {
            return true;
         }
      };
      this.dataSource.filter = filterValue;
   }

   openDialog(objetivo: any, row: any): void {
      let dialogRef = this.dialog.open(ObjetivoDialogComponent, {
         width: '500px',
         data: { objetivo: objetivo.objetivo }
      });

      dialogRef.afterClosed().subscribe(result => {
         if (!objetivo.objetivo) {
            objetivo.objetivo = result;
            this._ObjetivoService.save(objetivo)
               .subscribe(data => { });
         } else {
            objetivo.objetivo = result;
            this._ObjetivoService.actualizarObjetivo(objetivo)
               .subscribe(data => { });
         }

      });
   }

   nuevoIndicador(objetivo: any) {
      console.log(objetivo);
      let dialogRef = this.dialog.open(ObjetivoDialogComponent, {
         width: '500px',
         data: { objetivo: null }
      });

      dialogRef.afterClosed().subscribe(result => {
         if (!objetivo.indicadores) {
            console.log(objetivo);
            objetivo.indicadores = [result];
            this._ObjetivoService.actualizarObjetivo(objetivo)
               .subscribe(data => { });
         } else {
            console.log('asdf');
            objetivo.indicadores.push({numero:objetivo.indicadores.length+1  , descripcion: result});
            this._ObjetivoService.actualizarObjetivo(objetivo)
               .subscribe(data => { });
         }
         console.log(objetivo);
      });
   }

   editarIndicador(objetivo: any, indicador: any){
      let dialogRef = this.dialog.open(ObjetivoDialogComponent, {
         width: '500px',
         data: { objetivo: indicador.descripcion }
      });

      dialogRef.afterClosed().subscribe(result => {
            indicador.descripcion  = result;
            this._ObjetivoService.actualizarObjetivo(objetivo)
               .subscribe(data => { });
      });
   }

}

@Component({
   selector: 'app-objetivo-edit-dialog',
   templateUrl: 'objetivo-edit.dialog.html',
})
export class ObjetivoDialogComponent {

   objetivo: any;

   constructor(
      public dialogRef: MatDialogRef<ObjetivoDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
      this.objetivo = data.objetivo;
   }

   actualizar(): void {
      this.dialogRef.close(this.objetivo);
   }

}
