import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Objetivo } from './objetivos';

@Injectable()
export class ObjetivoService {

    private _getUrl = 'api/objetivo/';
    private _postUrl = 'api/objetivo/';
    private _putUrl = 'api/objetivo/';
    private _deleteUrl = 'api/objetivo/';

    constructor(private _http: Http) { }

    getobjetivos() {
        return this._http.get(this._getUrl)
            .map((response: Response) => response.json());
    }
    getobjetivosByCompromiso(id: String) {
        return this._http.get('api/objetivoByComprimiso/' + id)
            .map((response: Response) => response.json());
    }

    save(objetivo: Objetivo) {
        console.log(JSON.stringify(objetivo));
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._postUrl, JSON.stringify(objetivo), options)
            .map((response: Response) => response.json());
    }

    actualizarObjetivo(objetivo: Objetivo) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.put(this._putUrl + objetivo._id, JSON.stringify(objetivo), options)
            .map((response: Response) => response.json());
    }

    eliminarObjetivo(objetivo: Objetivo) {
        return this._http.delete(this._deleteUrl + objetivo._id)
            .map((response: Response) => response.json());
    }
}
