import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjIndicadoresComponent } from './obj-indicadores.component';

describe('ObjIndicadoresComponent', () => {
  let component: ObjIndicadoresComponent;
  let fixture: ComponentFixture<ObjIndicadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjIndicadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjIndicadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
