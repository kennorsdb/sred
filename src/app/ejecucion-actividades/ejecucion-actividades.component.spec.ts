import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjecucionActividadesComponent } from './ejecucion-actividades.component';

describe('EjecucionActividadesComponent', () => {
  let component: EjecucionActividadesComponent;
  let fixture: ComponentFixture<EjecucionActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjecucionActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjecucionActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
