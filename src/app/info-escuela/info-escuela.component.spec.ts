import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoEscuelaComponent } from './info-escuela.component';

describe('InfoEscuelaComponent', () => {
  let component: InfoEscuelaComponent;
  let fixture: ComponentFixture<InfoEscuelaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoEscuelaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoEscuelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
