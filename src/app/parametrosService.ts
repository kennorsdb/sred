import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Sede } from './sede';

@Injectable()
export class ParametrosService {

    constructor(private _http: Http) { }

    getSedes() {
        return this._http.get('api/sedes')
            .map((response: Response) => response.json());
    }

    getCentrosFuncionales() {
        return this._http.get('api/centrosFuncionales')
            .map((response: Response) => response.json());
    }


}
