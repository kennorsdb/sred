import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()
export class GraficosService{
    private _getUrl = 'api/graficos';
    /*private _postUrl = 'api/sede/';
    private _putUrl = 'api/sede/';
    private _deleteUrl = 'api/sede/';*/

    constructor(private _http: Http) { }

    getInformation() {
        return this._http.get(this._getUrl)
            .map((response: Response) => response.json());
    }
}
