import { Component, OnInit } from '@angular/core';
import { GraficosService } from './graficos.service';


@Component({
  selector: 'app-graficos',
  templateUrl: './graficos.component.html',
  styleUrls: ['./graficos.component.css'],
  providers: [GraficosService],
})
export class GraficosComponent implements OnInit {
	graficos:any;
   // Circular, progreso
  public doughnutChartLabels:string[] = ['Completadas', 'En Proceso', 'Pendientes'];
  public doughnutChartData:number[] = [1, 7, 2];
  public doughnutChartType:string = 'doughnut';

  //Barras, pOR AÑO
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['2016', '2017', '2018'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [1, 1, 0], label: 'Completadas'},
    {data: [1, 7, 8], label: 'No Completadas'}
  ];

  //evidencias por objetivo


  constructor(private graficosService: GraficosService) { }
  	ngOnInit() {
		this.graficosService.getInformation()
		.subscribe(resGraficosData => this.graficos = resGraficosData);
	}

  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

}
