import { User} from '../user';
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.css'],
})
export class UsuarioListComponent implements OnInit {

  @Input()
  usuarios: User;

  @Output()
  public SelectUsuario = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.usuarios);
  }

  onSelect(vid: User) {
    console.log('Lista:' + vid);
    this.SelectUsuario.emit(vid);
  }
}
