import { Proyecto } from '../proyecto/proyecto';
import { Objetivo } from '../obj-indicadores/objetivos';
import { User } from '../user';

export class Actividad {
    _id: String;
    objetivo: Objetivo;
    indicador: Number;
    numero: Number;
    descripcion: String;
    responsable: [String];
    fechaFinalizacion: Date;
    fechaCumplimiento: Date;
    progreso: String;
    observaciones: Array<Observacion>;
    evidencias: Array<Evidencia>;
    evaluaciones: Array<Evaluacion>;
}

export class Observacion {
    fecha: Date;
    usuario: User;
    descripcion: String;
}

export class Evidencia {
    tipoEvidencia: String;
    ruta: String;
    fecha: Date;
    usuario: User;
}

export class Evaluacion {
    usuario: User;
    calificacion: Number;
    observacion: String;
    periodo: Number;
    fecha: Date;
}
