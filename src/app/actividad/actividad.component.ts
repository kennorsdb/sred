import { Component, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ParametrosService } from '../parametrosService';
import { CompromisoService } from '../debilidades-causas/compromiso.service';
import { ProyectoService } from '../proyecto/proyecto.service';
import { ActividadService } from './actividad.service';
import { ObjetivoService } from '../obj-indicadores/objetivos.service';
import { Compromiso } from '../debilidades-causas/compromiso';

@Component({
   selector: 'app-actividad',
   templateUrl: './actividad.component.html',
   styleUrls: ['./actividad.component.css'],
   providers: [ProyectoService, ParametrosService, CompromisoService, ActividadService, ObjetivoService]
})
export class ActividadComponent implements OnInit {
   displayedColumns = ['No.', 'Indicadores', 'Descripcion', 'Responsable', 'Finalizacion'];
   manualSinaes: any;
   dataSource: any;

   dimensiones: any;
   dimension: any;
   componente: any;
   criterios: any;
   criterio: any;
   debilidades: any;
   debilidad: any;
   objetivos: any;

   centros: any;

   actividades: any;
   actividad: any;

   compromiso: any;
   constructor(private _ProyectoService: ProyectoService,
      private _ParametrosService: ParametrosService,
      private _CompromisoService: CompromisoService,
      private _ActividadService: ActividadService,
      private _ObjetivoService: ObjetivoService,
      public dialog: MatDialog) { }

   ngOnInit() {
      this._ProyectoService.getDimensiones()
         .subscribe(dimensionesData => {
            this.dimensiones = dimensionesData.dimensiones;
         });
      this._ParametrosService.getCentrosFuncionales()
         .subscribe(centrosData => {
            this.centros = centrosData.centros;
         });
      this._ProyectoService.getManualSinaes()
         .subscribe(manualData => {
            this.manualSinaes = manualData;
            this.dimension = manualData[0].Ndimension;
            this._ObjetivoService.getobjetivos()
               .subscribe(objetivosData => {
                  this.objetivos = objetivosData;
                  // Objetivos
                  this._ActividadService.getActividades().subscribe(actividadesData => {
                     this.actividades = actividadesData;
                     // tslint:disable-next-line:forin
                     for (let i in this.objetivos) {
                        let d = this.objetivos[i];
                        let unica = true;
                        for (let j in this.actividades) {
                           if (d._id === this.actividades[j].objetivo ||
                              d._id === this.actividades[j].objetivo._id) {
                              unica = false;
                              this.actividades[j].objetivo = d;
                           }
                        }
                        if (unica) {
                           for (let k in this.manualSinaes) {
                              if (this.manualSinaes[k]._id == d.compromisoMejora.manualSinaes) {
                                 d.compromisoMejora.manualSinaes = this.manualSinaes[k];
                                 break;
                              }
                           }
                           // tslint:disable-next-line:forin
                           for (let l in d.indicadores) {
                              this.actividades.push({
                                 'objetivo': d,
                                 'indicador': l.toString()
                              });
                           }
                        }
                     }
                     this.dataSource = new MatTableDataSource(this.actividades);
                     console.log(this.actividades);
                  });
               });
         });
   }

   async wait() {
      await new Promise(resolve => {
         setTimeout(400);
      });
   }

   applyDimension() {
      this.componente = this.dimension.componentes[0];
   }

   applyComponente() {
      let m = this.manualSinaes;
      this.criterios = [];
      for (let i in m) {
         if (m[i].dimension == this.dimension.numero &&
            m[i].componente == this.componente.numero) {
            this.criterios.push(m[i]);
         }
      }
   }

   applyCriterio() {
      this.applyFilter('a');
   }


   applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filterPredicate = (data, filter) => {
         console.log(data);
         console.log(this.criterio);
         const d = data.compromisoMejora.manualSinaes;
         if (d._id == this.criterio._id) {
            return true;
         }
      };
      this.dataSource.filter = filterValue;
   }

   guardarCambios() {
      // tslint:disable-next-line:forin
      for (let i in this.actividades) {
         if (this.actividades[i]._id) { // Se encuentra en la base de datos
            this._ActividadService.actualizarActividad(this.actividades[i])
               .subscribe(actividadData => this.actividades[i] = actividadData);
         } else { //es Nuevo
            this._ActividadService.save(this.actividades[i])
               .subscribe(actividadData => this.actividades[i] = actividadData);
         }
      }
   
      console.log(this.actividades);
   }


}
