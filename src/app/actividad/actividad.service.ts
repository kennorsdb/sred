import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Actividad } from './actividad';

@Injectable()
export class ActividadService {

    private _getUrl = 'api/actividad/';
    private _postUrl = 'api/actividad/';
    private _putUrl = 'api/actividad/';
    private _deleteUrl = 'api/actividad/';

    constructor(private _http: Http) { }

    getActividades() {
        return this._http.get(this._getUrl)
            .map((response: Response) => response.json());
    }

    save(actividad: Actividad) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._postUrl, JSON.stringify(actividad), options)
            .map((response: Response) => response.json());
    }

    actualizarActividad(actividad: Actividad) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        console.log(actividad);
        console.log(JSON.stringify(actividad));
        return this._http.put(this._putUrl + actividad._id, JSON.stringify(actividad), options)
            .map((response: Response) => response.json());
    }


    eliminarActividad(actividad: Actividad) {
        return this._http.delete(this._deleteUrl + actividad._id)
            .map((response: Response) => response.json());
    }

} 
