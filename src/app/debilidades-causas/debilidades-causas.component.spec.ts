import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebilidadesCausasComponent } from './debilidades-causas.component';

describe('DebilidadesCausasComponent', () => {
  let component: DebilidadesCausasComponent;
  let fixture: ComponentFixture<DebilidadesCausasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebilidadesCausasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebilidadesCausasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
