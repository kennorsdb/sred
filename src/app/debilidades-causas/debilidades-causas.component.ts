import { Component, OnInit, ViewChild } from '@angular/core';
import { ProyectoService } from '../proyecto/proyecto.service';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ParametrosService } from '../parametrosService';
import { CompromisoService } from './compromiso.service';
import { Compromiso } from './compromiso';

@Component({
  selector: 'app-debilidades-causas',
  templateUrl: './debilidades-causas.component.html',
  styleUrls: ['./debilidades-causas.component.css'],
  providers: [ProyectoService, ParametrosService, CompromisoService]
})
export class DebilidadesCausasComponent implements OnInit {
  manualSinaes: any;
  dimensiones: any;
  dimension: any;
  componente: any;
  criterios: any;
  criterio: any;
  sedes: any;
  sede: any;
  debilidad: any;
  causa: any;

  compromiso: any;

  constructor(private _ProyectoService: ProyectoService,
    private _ParametrosService: ParametrosService,
    private _CompromisoService: CompromisoService) { }

  ngOnInit() {
    this._ProyectoService.getManualSinaes()
      .subscribe(manualData => {
        this.manualSinaes = manualData;
        console.log(manualData);
        this.dimension = manualData[0].Ndimension;
      });

    this._ProyectoService.getDimensiones()
      .subscribe(dimensionesData => {
        this.dimensiones = dimensionesData.dimensiones;
      });
    this._ParametrosService.getSedes()
      .subscribe(sedesData => {
        this.sedes = sedesData;
      });

  }

  applyDimension() {
    this.componente = this.dimension.componentes[0];
  }

  applyComponente() {
    let m = this.manualSinaes;
    this.criterios = [];
    for (let i in m) {
      if (m[i].dimension == this.dimension.numero &&
        m[i].componente == this.componente.numero) {
        this.criterios.push(m[i]);
      }
    }
  }

  nuevoCompromiso() {
    const compromiso = new Compromiso();
    if (this.debilidad && this.causa && this.criterio) {
      compromiso.debilidad = this.debilidad;
      compromiso.causa = this.causa;
      compromiso.programa = [this.sede];
      compromiso.manualSinaes = this.criterio._id;

      this._CompromisoService.save(compromiso)
      .subscribe(nuevoData => {
        this.compromiso = nuevoData;
      });
    }
  }

  borrarCompromiso() {
    if ( this.compromiso ) {
      this._CompromisoService.eliminarCompromiso(this.compromiso).subscribe(nuevoData => {
        this.compromiso = nuevoData;
        console.log(this.compromiso);
        this.compromiso = null;
      });
    }
  }

  actualizarCompromiso() {
    if (this.debilidad && this.causa && this.criterio) {
      this.compromiso.debilidad = this.debilidad;
      this.compromiso.causa = this.causa;
      this.compromiso.causa = this.causa;
      this.compromiso.programa = [this.sede];
      this.compromiso.manualSinaes = this.criterio._id;

      this._CompromisoService.actualizarCompromiso(this.compromiso)
      .subscribe(nuevoData => {
        this.compromiso = nuevoData;
      });
    }

  }

  limpiarCompromiso() {
    this.dimension = null;
    this.componente = null;
    this.criterio = null;
    this.sede = null;
    this.debilidad = null;
    this.causa = null;
    this.compromiso = null;
  }

}
