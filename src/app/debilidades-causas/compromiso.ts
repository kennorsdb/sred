import { Proyecto } from '../proyecto/proyecto';

export class Compromiso {
    _id: String;
    programa: Array<String>;
    debilidad: String;
    causa: String;
    manualSinaes: Proyecto;
}
