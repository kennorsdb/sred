import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Compromiso } from './compromiso';

@Injectable()
export class CompromisoService {

    private _getUrl = 'api/compromiso/';
    private _postUrl = 'api/compromiso/';
    private _putUrl = 'api/compromiso/';
    private _deleteUrl = 'api/compromiso/';

    constructor(private _http: Http) { }

    getCompromisos() {
        return this._http.get(this._getUrl)
            .map((response: Response) => response.json());
    }

    save(compromiso: Compromiso) {
        console.log(JSON.stringify(compromiso));
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._postUrl, JSON.stringify(compromiso), options)
            .map((response: Response) => response.json());
    }

    actualizarCompromiso(compromiso: Compromiso) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.put(this._putUrl + compromiso._id, JSON.stringify(compromiso), options)
            .map((response: Response) => response.json());
    }

    getCompromisoByManualSinaes(id: String) {
        return this._http.get('api/compromisoByManualSinaes/' + id)
            .map((response: Response) => response.json());
    }

    eliminarCompromiso(compromiso: Compromiso) {
        return this._http.delete(this._deleteUrl + compromiso._id)
            .map((response: Response) => response.json());
    }

}
