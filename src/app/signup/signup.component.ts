import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { SignUpService } from './signup.service';
import { ParametrosService } from '../parametrosService';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [SignUpService, ParametrosService]
})


export class SignUpComponent implements OnInit {

  user: User;
  sedes: any;

  constructor(private _signUpService: SignUpService, private parametrosService: ParametrosService) { }

  ngOnInit() {
    this.user = null;
    this.parametrosService.getSedes()
    .subscribe(sedesData => {this.sedes = sedesData; console.log(this.sedes); });
  }

  onSubmit(newUser) {
    console.log(newUser);
    this._signUpService.registrarUsuario(newUser)
    .subscribe(userData => this.user = userData);
  }
}
