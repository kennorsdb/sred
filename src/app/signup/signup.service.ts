import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { User } from '../user';

@Injectable()
export class SignUpService {

    private _getUrl = 'api/signup';
    private _postUrl = 'api/signup';
    private _putUrl = 'api/signup';
    private _deleteUrl = 'api/signup';

    constructor(private _http: Http) { }

    registrarUsuario(user: User) {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._postUrl, JSON.stringify(user), options)
            .map((response: Response) => response.json());
    }

    getProfile() {
        return this._http.get('api/profile')
            .map((response: Response) => response.json());
    }
}
