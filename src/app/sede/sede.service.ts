import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Sede } from '../sede';

@Injectable()
export class SedeService {

    private _getUrl = 'api/sedes';
    private _postUrl = 'api/sede/';
    private _putUrl = 'api/sede/';
    private _deleteUrl = 'api/sede/';

    constructor(private _http: Http) { }

    registrarSede(sede: Sede) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._postUrl, JSON.stringify(sede), options)
            .map((response: Response) => response.json());
    }

    getSedes() {
        return this._http.get(this._getUrl)
            .map((response: Response) => response.json());
    }

    updateSede(sede: Sede) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.put(this._putUrl + sede._id, JSON.stringify(sede), options)
          .map((response: Response) => response.json());
    }

}
