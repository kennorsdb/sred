import { Component, OnInit } from '@angular/core';
import { SedeService } from './sede.service';
import { Sede} from '../sede';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Component({
  selector: 'app-sede',
  templateUrl: './sede.component.html',
  styleUrls: ['./sede.component.css'],
  providers: [SedeService]
})
export class SedeComponent implements OnInit {

	sedes: any;
	sede: any;
	dataSource: any;

	constructor(private sedeService: SedeService) { }
	ngOnInit() {
		this.sedeService.getSedes()
		.subscribe(resSedeData => this.sedes = resSedeData);
	}

	onSubmit(newSede) {
		console.log(newSede);
		this.sedeService.registrarSede(newSede)
		.subscribe(sedeData => this.sede = sedeData);

		this.sedeService.getSedes()
		.subscribe(resSedeData => this.sedes = resSedeData);
  }

}


