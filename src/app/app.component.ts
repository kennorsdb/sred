import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { SignUpService } from './signup/signup.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [SignUpService]
})


export class AppComponent implements OnInit{
  title = 'S-RED';

  user: User;

  constructor(private _signUpService: SignUpService) { }

  ngOnInit(): void {
    this._signUpService.getProfile()
    .subscribe(userData => this.user = userData);
  }
}
