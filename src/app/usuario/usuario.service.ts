import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { User } from '../user';

@Injectable()
export class UsuarioService {

    private _getUrl = 'api/usuarios';
    private _postUrl = 'api/usuario/';
    private _putUrl = 'api/usuario/';
    private _deleteUrl = 'api/usuario/';

    constructor(private _http: Http) { }

    registrarUsuario(user: User) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._postUrl, JSON.stringify(user), options)
            .map((response: Response) => response.json());
    }

    getUsers() {
        return this._http.get(this._getUrl)
            .map((response: Response) => response.json());
    }

    updateUsuario(usuario: User) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.put(this._putUrl + usuario._id, JSON.stringify(usuario), options)
          .map((response: Response) => response.json());
    }

}
