import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './usuario.service';
import { User} from '../user';
@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
  providers: [UsuarioService]
})

export class UsuarioComponent implements OnInit {
  usuarios: any;
  usuario: any;

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.usuarioService.getUsers()
      .subscribe(resUsuarioData => this.usuarios = resUsuarioData);
  }

  onSelectUsuario($usuario: User) {
    this.usuario = $usuario;
  }

  onUpdateUsuarioEvent(usuario: User) {
    this.usuarioService.updateUsuario(usuario)
    .subscribe(resUsuarioData => this.usuario = resUsuarioData);
  }


}
