# Plan de Pruebas de la Primera Iteración

## Identificador del Plan de Pruebas

Plan de Pruebas Maestro para el Sistema SRED, iteración 1. (SRED-01-a).

## Referencias

[1] Miranda, Martínez, Obando. (2018) Visión y Alcances de S-RED. Instituto Tecnológico de Costa Rica, 2017.
[2] Sistema Nacional de Acreditación de la Educación Superior. (2009). Manual de Acreditación Oficial de Carreras de Grado del Sistema Nacional de Acreditación de la Educación Superior. San José, Costa Rica. Retrieved from <https://www.sinaes.ac.cr/documentos/Manual_de_Acreditacion_Oficial_de_Carreras_de_Grado.pdf>
[3] Sistema Nacional de Acreditación de la Educación Superior. (2012). Guía para Elaborar y Revisar el Informe de Avance de Cumplimiento del Compromiso de Mejora. San José, Costa Rica. Retrieved from <https://www.sinaes.ac.cr/documentos/Guia_elaborar_y_revisar_ACCM.pdf>
[4] Whyte, A. (2014). Google JavaScript Style Guide. Google.Github.Io, 1–24. Retrieved from <https://google.github.io/styleguide/jsguide.html>
[5] Miranda, Martínez, Obando. (2018) Especificación de Requerimientos de Software del Sistema S-RED    . Instituto Tecnológico de Costa Rica, 2017.

## Introducción

Este documento describe el conjunto de pruebas que se ejecutarán para la primera iteración del _"Sistema de Recolección de Evidencia Documental **(S-RED)**"_, en concordancia con los objetivos planteados en el documento _"Visión y Alcances"_ [1], y se establecen los indicadores de calidad para los componentes de software que se realizarán en esta etapa del proyecto.

La primera iteración consiste en la implementación y prueba del modelo de la base de datos, además de los casos de uso relacionados con el registro y el acceso del _"Compromiso de Mejora"_ [2].

Para el diseño de las pruebas se utilizan los requerimientos funcionales y no funcionales del documento _"Especificación de Requerimientos de Software del Sistema **S-RED**"_ [5].No se pretende crear pruebas exhaustivas del sistema, sino que éstas comprueben el funcionamiento de las características esenciales en el cumplimiento de lo especificado por el usuario.

## Elementos de Prueba (Funciones)

Los componentes que se desean probar son los siguientes:

1. Servidor de Base de Datos Mongo 3.6
2. Servidor HTML Express 4.16.
3. Funcionamiento del Api sobre protocolo Http.
4. Funcionamiento del framework para la aplicación web AngularJS 2.
5. Módulo de gestión de usuarios.

## Problemas de Riesgo del Software

A continuación se detallan los riesgos que se  han identificado para la ejecución de este Plan e Pruebas.

1. Fallos del servidor externo de la base de datos.
2. Problemas de comunicación con el servidor HTML.
3. Bajo rendimiento del equipo para la ejecución de pruebas.
4. Casos de prueba que no se tomaron en cuenta.
5. Fallos profundos del sistema que impidan la ejecución en el tiempo esperado.

## Características que serán probadas

A continuación se detallan las funcionalidades y requerimientos que se desean comprobar en este Plan de Pruebas:

### Base de Datos y API

1. La base datos es funcional, satiface las necesidades del sistema cumpliento con los requerimientos de rendimiento [5, pág. 44] y de seguridad [5, pág. 44].
2. El API contiene las funciones necesarias para el acceso a los datos del usuario, el registro e inicio de sesión.

### Caso de Usos Relacionados con el Registro de Usuarios

1. El sistema se debe conectar exitosamente a la base de datos (CU-1, REQ-1).
2. El Api de la aplicación debe enlistar los usuarios pendientes de aceptación (CU-1, REQ-2).
3. El sistema debe permitir el registro e inicio de sesión de un usuario (CU-2,REQ-1).
4. El sistema debe listar los datos de un usuario (CU-2,REQ-2).
5. Se debe poder modificar los datos de un usuario (CU-2,REQ-3) y guardar los cambios (CU-2,REQ-4).

## Características que no serán probadas

El presente Plan de Pruebas no contempla probar las siguientes características.

1. Máximo de usuarios conectados en el sistema.
2. Tiempo de respuesta del servidor a máxima carga.

## Enfoque

Esta sección describe los principios y estrategias que guiarán el proceso de ejecución de las pruebas.

### Principios de Prueba

1. Las pruebas se centrarán en cumplir con los objetivos del negocio, la eficiencia de costos y la calidad.
2. Habrá procedimientos comunes y consistentes para todos los equipos que respaldan las actividades de prueba.
3. Los procesos de prueba estarán bien definidos, pero serán flexibles, con la capacidad de cambiar según sea necesario.
4. Las actividades de prueba se basarán en las etapas previas para evitar la duplicación de esfuerzos redundantes.
5. El ambiente y los datos de la prueba emularán un entorno de producción tanto como sea posible.
6. La prueba será una actividad repetible, cuantificable y mensurable.
7. La prueba se dividirá en distintas fases, cada una con objetivos y metas claramente definidos.

### Estrategias para las Pruebas

Para cumplir con los objetivos y principios propuestos, se realizarán las siguientes estrategias:

1. Los datos serán los suministrados por la Escuela de Computación.
2. Se crearán 10 cuentas de usuario de forma manual y utilizando la interfaz gráfica. Estos usuarios se distribuirán en distintas Sedes y con diferentes roles.