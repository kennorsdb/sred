
# Introducción
En este informe se plantea las decisiones técnicas de diseño del Sistema de Recolección de Evidencia Documental (S-RED) para la Escuela de Computación del Instituto Tecnológico de Costa Rica, que permitirá mejorar los procesos de evaluación del Compromiso de Mejora suscrito por la Escuela con el Sistema Nacional de Acreditación de la Educación Superior.

## Propósito
Este documento permite una visión exhaustiva de la arquitectura empleada en S-RED haciendo uso de diferentes diagramas para su representación, en los cuales se muestran y convergen las decisiones más significativas tomadas a la hora de diseñar el sistema.

## Alcance
En las siguientes páginas se pretende describir la arquitectura en general, así como los estándares utilizados y cómo se distribuyen los distintos componentes, lo que implica que no se describe la metodología de dessarrollo empleado o la organización del trabajo en sí mismo.

## Definiciones, Acrónimos y Abreviaciones

## Descripción del Documento
La primera sección hace una referencia a las generalidades de este documento, en la segunda sección se mostrará en grandes rasgos cuál es la representación de la arquitectura del software. En la tercera parte se definirán los objetivos y las limitaciones del software en sí mismo. En las secciones del 4 al 8 se describen a detalle los diferentes componentes del programa: la vista de proceso, la vista de despliegue y la vista de implementación. En la sección 9 se analizarán cómo se distribuyen los datos en el sistema, mientras que en las secciones 10 y 11 se describen los criterios de calidad del mismo.

