
Introducción
=========================

Propósito
-------------------------
En este documento se describen los requerimientos funcionales y no funcionales para la primera versión (1.0) de **S-RED**, un sistema enfocado al seguimiento del Compromiso de Mejora suscrito por la Escuela de Computación del Instituto Tecnológico de Costa Rica con el **SINAES**, para la acreditación de las carreras.

Convenciones Tipográficas
-------------------------
- Los nombres propios serán resaltados en **letra negrita.**
No se estrablece ninguna convención especial en este documento.

Audiencia
-------------------------
Este documento está destinado para ser utilizado por los miembros del equipo de desarrollo que diseñará, implemantará y verificará el correcto funcionamiento del sistema. Además de aquellas personas implicadas en el mantenimiento a mediano y largo plazo del sistema.

Alcance del Proyecto
------------------------
**S-RED** tendrá un impacto directo en la eficienca del proceso de recolección de evidencias necesarias para los procesos de acreditación de la Escuela de Computación, al permitir almacenar y organizar cada documento, relacionado con los criterios definidos por el **SINAES**, con herramientas que faciliten el seguimiento y control en la ejecución de las actividades. En el documento *Visión y Alcance de S-RED* [1] se especifican a detalle las características principales del software.

Referencias
------------------------
[1] Miranda, Martínez, Obando. (2018) *Visión y Alcances de S-RED*. Instituto Tecnológico de Costa Rica, 2017.

[2] Sistema Nacional de Acreditación de la Educación Superior. (2009). *Manual de Acreditación Oficial de Carreras de Grado del Sistema Nacional de Acreditación de la Educación Superior*. San José, Costa Rica. Retrieved from https://www.sinaes.ac.cr/documentos/Manual_de_Acreditacion_Oficial_de_Carreras_de_Grado.pdf

[3] Sistema Nacional de Acreditación de la Educación Superior. (2012). *Guía para Elaborar y Revisar el Informe de Avance de Cumplimiento del Compromiso de Mejora*. San José, Costa Rica. Retrieved from https://www.sinaes.ac.cr/documentos/Guia_elaborar_y_revisar_ACCM.pdf

`4] Whyte, A. (2014). Google JavaScript Style Guide. Google.Github.Io, 1–24. Retrieved from https://google.github.io/styleguide/jsguide.html

Descripción General
========================

Perspectiva del Producto
------------------------
**S-RED** es un nuevo sistema que responde a la necesidad de la **Escuela de Computación** para el seguimiento del proceso de acreditación de sus carreras adscritas. El Diagrama de Contexto *(Figura 1)* ilustra las entidades externas y las interfaces del sistema para la versión 1.0. Se espera una alta acogida de los funcionarios y evaluadores, y se abre la posibilidad para la incorporación de nuevas herramientas en las versiones siguientes.

![***Insertar Diagrama de Contexto*****](./diagrama-Contexto.png "Figura 1. Diagrama de Contexto"){:height="36px" width="36px"}
 

Características del Producto
----------------------------
La versión 1.0 de **S-RED** contará, al menos, con las siguientes características:
1. La aplicación web será accesible en todo momento, o lugar, siempre que se tenga acceso a Internet.
2. El  programa  tendrá  una  estética  minimalista  y  agradable,  de  acuerdo  con  los estándares del cliente. 
3. Existencia de una distinción de los roles: moderador, colaborador y evaluador. De esta manera se administra la interacción, de cada uno de ellos, con la base de datos de la aplicación y sus respectivos documentos.
4. La aplicación presentará un listado con todos los estatutos establecidos en el Acuerdo de Mejora del **SINAES**, en conjunto con una descripción, sus respectivas dependencias y un estado que determina si el problema ya fue resuelto, o se encuentra pendiente de tratar.
5. Capacidad de crear, actualizar y consultar los objetivos de mejora.
6. Se integrará la creación, actualización y consulta de evidencias documentales para un determinado objetivo de mejora de **SINAES**.
7. Funcionalidad de carga y descarga de documentos, para cada uno de los objetivos de mejora

Roles de Usuarios y Características
-----------------------------------

| Clase de Usuario | Descripción                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Administrador    | Tiene el más alto nivel de privilegios del sistema. Se espera que existan más de dos administradores a discreción del *Sponsor* del Sistema. Puede gestionar a los usuarios (eliminar, reestablecer contraseña, cambiar foto de perfil); puede agregar toda la información propia del Compromiso de Mejora, actualizar los criterios, ingresar evidencias, definir el progreso de las actividades, generar todos los tpos de reportes. |
| Funcionario      | Son los encargados de registrar las evidencias documentales a cada actividad, pueden generar los informes que el *Sponsor* define, y acceder a la información detallada del **Compromiso de Mejora**.                                                                                                                                                                                                                                  |
| Evaluador        | Puede acceder a una tabla con información de los avances de las actividades y agregar comentarios; puede generar reportes específicos definidos por el *Sponsor*.                                                                                                                                                                                                                                                                      |


Entorno Operativo
---------------------------------
- **EO-01:** **S-RED** debe operar correctamente en los navegadores *Opera*, *Google*, *Chrome*, *Microsoft Edge*, *Mozilla Firefox* y *Safari*.
- **EO-02:** El código de S-RED se debe ejecutar correctamente en la versión 6.10.3 de *Node.Js*
- **EO-03:** El código de S-RED debe utilizar correctamente las características de *Angular 2*.

Restricciones de Diseño e Implementación
-----------------------------------------
- **RD-01:** **S-RED** se debe ajustar a los criterios planteados por **SINAES** en el *Manual de Acreditación Oficial de Carreras de Grado del Sistema Nacional de Acreditación de la Educación Superior* [2]. 
- **RD-02:** El Compromiso de Mejora debe seguir el *Guía para Elaborar y Revisar el Informe de Avance de Cumplimiento del Compromiso de Mejora* [3].
- **RD-03:** Se seguirá la convención *Google JavaScript Style Guide* [4], como estilo de formato del código.

Documentación Para el Usuario
-----------------------------------------
- **Du-01:** Manual de Usuario
- **Du-02:** Visión y Alcance del Producto [1]
- **Du-03:** 
- **Du-04:** 


Suposiciones y Dependencias
-----------------------------------------
- **SP-01:** Se asume que el los servidores que serán habilitados cuentan con un sistema compatible con NodeJs y que cuenta una adecuada configuración.
- **SP-02:** Los lineamientos del **SINAES** se mantendrán vigentes mientras se desarrolla el software.
- **SP-03:** ****
- **SP-04:** 
