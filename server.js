
// Librerías Básicas
const express       = require('express');
const bodyParser    = require('body-parser');
const path          = require('path');
const passport      = require('passport');
const flash         = require('connect-flash');
const cookieParser  = require('cookie-parser');
const session       = require('express-session');
const mongoose      = require('mongoose');
// WebPack
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');


// Dependencias internas del programa
const port = 4200;
const app = express();


app.use(express.static(path.join(__dirname, 'dist')));

app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); 


// Inicialización del control de la Sesión de los usuarios.
require('./server/routes/passport')(passport);                  // Se configura passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session());                                    // persistent login sessions


// Se configura MongoDB
const db = "mongodb://sred:qwertyui@ds125489.mlab.com:25489/sred";
mongoose.Promise = global.Promise;
mongoose.connect(db, function(err){
    if(err){
        console.error("Error de conexión a la Base de datos: " + err);
    }
});


// Se configuran las rutas
require('./server/routes/routes')(app, passport);



// Se inicia el servidor Web
app.listen(port, function(){
    console.log("Server running on localhost:" + port);
});

