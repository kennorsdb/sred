# TareaMean

## String de Conexión

Para ver los datos y probar las funcionalidades se utiliza el siguiente string de conexión:

mongodb://sred:qwertyui@ds125489.mlab.com:25489/sred 

## Para correr el server

### Instalar todas las librerías
Se ejecuta dentro de la carpeta SRED:
- npm -g install
- npm install -g @angular/cli 

### Compilar el código fuente:
- ng build

### Iniciar el servidor 
- node server

Se debería conectar en el navegador en la dirección

http:\\127.0.0.1:4200