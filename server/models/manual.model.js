const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const manualSinaesSchema = new Schema({
	dimension: String,
	componente: String,
	criterio: String,
	descripcion: String,
	estandares: [{
		numero: String,
		descripcion: String
	}],

	evidenciasEsperadas: [{
		numero: String,
		descripcion: String
	}]
});

module.exports = mongoose.model('manual', manualSinaesSchema, 'manuales');