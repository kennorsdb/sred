const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const actividadSchema = new Schema({
	objetivo: {type: Schema.Types.ObjectId, ref: 'objetivo'},
	indicador: Number,
	numero: Number,
	descripcion: String,
	responsable: [String],
	fechaFinalizacion: Date,
	fechaCumplimiento: Date,
	progreso: String,
	observaciones: [{
		fecha: Date,
		usuario: {type: Schema.Types.ObjectId, ref: 'usuario'},
		descripcion: String
	}],
	evidencias: [{
		tipoEvidencia: String,
		ruta: String,
		fecha: Date,
		usuario: {type: Schema.Types.ObjectId, ref: 'usuario'}
	}],
	evaluaciones: [{
		usuario: {type: Schema.Types.ObjectId, ref: 'usuario'},
		calificacion: Number,
		observacion: String,
		periodo: Number,
		fecha: Date
	}]
});

module.exports = mongoose.model('actividad', actividadSchema, 'actividades');