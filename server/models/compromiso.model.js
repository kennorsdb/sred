const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const compromisoMejoraSchema = new Schema({
	manualSinaes: {type: Schema.Types.ObjectId, ref: 'manual'},
	programa: [String],
	debilidad: String,
	causa: String,
});

module.exports = mongoose.model('compromiso', compromisoMejoraSchema, 'compromisos');
