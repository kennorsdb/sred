const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sedeSchema = new Schema({
	nombre: String,
});

module.exports = mongoose.model('sede', sedeSchema, 'sedes');
