const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const objetivoSchema = new Schema({
	compromisoMejora: {type: Schema.Types.ObjectId, ref: 'compromiso'},
	objetivo: String,
	indicadores: [{
		numero: Number,
		descripcion: String,
		actividades: [{type: Schema.Types.ObjectId, ref: 'actividad'}]
	}]

});

module.exports = mongoose.model('objetivo', objetivoSchema, 'objetivos');