const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
var path = require('path');
const Actividad = require('../models/actividad.model');
var multer = require('multer');

var DIR = './evidencias/';


const db = "mongodb://sred@ds125489.mlab.com:25489/sred";
mongoose.Promise = global.Promise;

mongoose.connect(db, function (err) {
    if (err) {
        console.error("Error! " + err);
    }
});

router.get('/actividad', function (req, res) {
    console.log('Get request for Actividad');
    Actividad.find({})
        .exec(function (err, Actividad) {

            if (err) {
                console.log("Error retrieving Actividad");
            } else {
                res.json(Actividad);
            }
        });
});


router.get('/actividad/:id', function (req, res) {
    console.log('Get request for a single Actividad');
    Actividad.findById(req.params.id)
        .exec(function (err, actividad) {
            if (err) {
                console.log("Error retrieving Actividad");
            } else {
                res.json(actividad);
            }
        });
});


router.post('/actividad', function (req, res) {
    console.log('Post Actividad');
    var newActividad = new Actividad();
    newActividad.numero = req.body.numero;
    newActividad.descripcion = req.body.descripcion;
    newActividad.responsable = req.body.responsable;
    newActividad.fechaFinalizacion = req.body.fechaFinalizacion;
    newActividad.fechaCumplimiento = req.body.fechaCumplimiento;
    newActividad.progreso = req.body.progreso;
    newActividad.observaciones = req.body.observaciones;
    newActividad.evidencias = req.body.evidencias;
    newActividad.evaluaciones = req.body.evaluaciones;
    newActividad.objetivo = req.body.objetivo;
    newActividad.indicador = req.body.indicador;

    newActividad.save(function (err, insertedActividad) {
        if (err) {
            console.log('Error al guardar Actividad');
        } else {
            res.json(insertedActividad);
        }
    });
});

router.put('/actividad/:id', function (req, res) {
    console.log('Actualizar Actividad');
    console.log(req.body);
    Actividad.findByIdAndUpdate(req.params.id,
        {
            $set: {
                numero: req.body.numero,
                descripcion: req.body.descripcion,
                responsable: req.body.responsable,
                fechaFinalizacion: req.body.fechaFinalizacion,
                fechaCumplimiento: req.body.fechaCumplimiento,
                progreso: req.body.progreso,
                observaciones: req.body.observaciones,
                evidencias: req.body.evidencias,
                evaluaciones: req.body.evaluaciones,
                observacion: req.body.observacion,
                indicador: req.body.indicador
            }
        },
        {
            new: true
        },
        function (err, updatedActividad) {
            if (err) {
                res.send("Error al actualizar Actividad");
            } else {
                res.json(updatedActividad);
            }
        }

    );
});

router.delete('/actividad/:id', function (req, res) {
    console.log('Deleting a Actividad');
    Actividad.findByIdAndRemove(req.params.id, function (err, deletedActividad) {
        if (err) {
            res.send("Error deleting Actividad");
        } else {
            res.json(deletedActividad);
        }
    });
});

router.post('/actividad/:id/:dim/:comp/:cr/:evid/upEvidencia/', function (req, res, next) {
    var path = '';
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            var shell = require('shelljs');
            shell.mkdir('-p', './evidencias/' + req.params.dim + '/' + req.params.comp + '/' + req.params.cr + '/');
            cb(null, './evidencias/' + req.params.dim + '/' + req.params.comp + '/' + req.params.cr + '/');
        },
        filename: function (req, file, cb) {
            cb(null, '[E' + req.params.evid + '] - ' + file.originalname);

            console.log(req.params.cr);
        }
    })

    var upload = multer({ storage: storage }).single('photo');

    upload(req, res, function (err) {
        if (err) {
            // An error occurred when uploading
            console.log(err);
            return res.status(422).send("an Error occured")
        }
        // No error occured.
        path = req.file.path;
        console.log(req.file);

        Actividad.findById(req.params.id)
            .exec(function (err, Actividad) {
                if (err) {
                    console.log("Error retrieving Actividad");
                } else {
                    Actividad.evidencias.push({
                        tipoEvidencia: 'file',
                        ruta: path,
                        fecha: Date.now(),
                        usuario: req.user
                    });
                    Actividad.save();
                    //res.json(Actividad);
                }
            });


        return res.send("Upload Completed for " + path);
    });
});

router.get('/graficos', function(req, res){
    console.log('Get request for Actividad');
    Actividad.aggregate( {"$group" :  {_id :"$objetivo", count : { $sum : 1} } } )
    .exec(function(err, Actividad){
        if (err){
            console.log("Error retrieving Actividad");
        }else {
            console.log(json(Actividad));
            res.json(Actividad);
        }
    });
});

module.exports = router; 
