const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const path = require('path');

const manualSinaesApi = require('./apiManual');
const UsuarioApi = require('./apiUsuario');
const CompromisoApi = require('./apiCompromiso');
const Dimensiones = require('../models/dimensiones');
const ObjetivoApi = require('./apiObjetivo');
const ActividadApi = require('./apiActividad');
const SedeApi = require('./apiSede');
//const GraficosApi = require('./apiGraficos');


router.use('/', manualSinaesApi);
router.use('/', UsuarioApi);
router.use('/', CompromisoApi);
router.use('/', ObjetivoApi);
router.use('/', ActividadApi);
router.use('/', SedeApi);
//router.use('/',GraficosApi);


router.get('/sedes', function (req, res) {
    res.json({
        "sedes": [
            "ATI",
            "IC-CA",
            "IC-AL",
            "IC-SJ",
            "IC-SC"
        ]
    });
});

router.get('/centrosFuncionales', function (req, res) {
    res.json({
        "centros":[
            "Coordinador de Área",
            "Comisión enlace con la industria",
            "Consejo de Área",
            "Director de Escuela",
            "Comisión para reforzar los programas con respeto al abordaje de los cursos en los temas éticos",
            "Consejo de Escuela",
            "Coordinadores de Carrera",
            "Gestión Curricular",
            "Comisión del plan de desarrollo académico y profesional",
            "Comisión del plan de desarrollo académico y personal",
            "Docentes de Área",
            "Consejo de Rectoría",
            "Consejo de Unidad",
            "Comisión de Seguridad",
            "Coordinador de la Oficina de TI de la Escuela",
            "Coordinadores de cursos",
            "Comisión de Acreditación",
            "Asesor del CEDA",
            "CEDA",
            "Vicerrector de Docencia",
            "Comité Académico",
            "Coordinador del CIC",
            "Comité Científico del Área",
            "Comité Técnico de la Escuela",
            "Coordinador de Práctica",
            "Dirección de la Escuela",
            "Investigadores",
            "Coordinador del Centro de Investigación"
        ]
    });
});

router.get('/dimensiones', function (req, res) {
    res.json(Dimensiones);
});

module.exports = router; 