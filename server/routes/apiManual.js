const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const path = require('path');
const manual = require('../models/manual.model');
const dimensiones = require('../models/dimensiones.json')


const db = "mongodb://sred@ds125489.mlab.com:25489/sred";
mongoose.Promise = global.Promise;

mongoose.connect(db, function (err) {
    if (err) {
        console.error("Error! " + err);
    }
});

router.get('/manualSinaes', function (req, res) {
    console.log('Get request for Manual Sinaes');
    manual.find().lean()
        .exec(function (err, manual) {
            if (err) {
                console.log(err);
                console.log("Error retrieving Manual");
            } else {
                NManual = manual;
                // Cambia el nombre de cada dimensión
                for (i in manual) {
                    let nD = dimensiones.dimensiones.find(
                        o => o.numero.toString() === manual[i].dimension
                    );
                    manual[i].Ndimension = nD.nombre;
                    numComponente = manual[i].componente.toString();
                    manual[i].Ncomponente = nD.componentes[numComponente-1].nombre; 
                     
                }
                res.json(NManual);
            }
        });
});

router.get('/parametros', function (req, res) {
    console.log('Solicitud de Parámetros');
    manual.find({})
        .exec(function (err, manual) {
            if (err) {
                console.log("Error retrieving Manual");
            } else {
                res.json(manual);
            }
        });
});

router.get('/manualSinaes/:id', function (req, res) {
    console.log('Get request for a single criterio');
    manual.findById(req.params.id)
        .exec(function (err, manual) {
            if (err) {
                console.log("Error retrieving criterio");
            } else {
                res.json(manual);
            }
        });
});

module.exports = router; 
