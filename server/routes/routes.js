const path = require('path');
const api = require('./api');


var rootPath = path.join(__dirname + '../../..');

module.exports = function (app, passport) {
    app.use('/api', api);

    app.post('/api/signup', passport.authenticate('local-signup', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/signup' // redirect back to the signup page if there is an error
    }));

    app.post('/api/login', passport.authenticate('local-login', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/signup' // redirect back to the signup page if there is an error
    }));

    app.get('/api/logout', function(req, res){
        req.logout();
        res.redirect('/');
      });

    app.get('/api/profile', isLoggedIn, function (req, res) {
        res.send(req.user);
    });

    app.get('/api/profile', isLoggedIn, function (req, res) {
        res.send(req.user);
    });

     app.get('*', (req, res) => {
         res.sendFile(rootPath +  '/src/index.html');
     });



    }

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
};