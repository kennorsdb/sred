const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
var path = require('path');
const Compromiso = require('../models/compromiso.model');



const db = "mongodb://sred@ds125489.mlab.com:25489/sred";
mongoose.Promise = global.Promise;

mongoose.connect(db, function (err) {
  if (err) {
    console.error("Error! " + err);
  }
});

router.get('/compromiso', function (req, res) {
  console.log('Get request for compromiso');
  Compromiso.find({}).populate('manualSinaes')
    .exec(function (err, compromiso) {

      if (err) {
        console.log("Error retrieving compromiso");
      } else {
        res.json(compromiso);
      }
    });
});

router.get('/compromisoByManualSinaes/:id', function (req, res) {
  console.log('Get request for compromiso');
  Compromiso.find({ "manualSinaes": req.params.id } ).populate('manualSinaes')
    .exec(function (err, compromiso) {
      if (err) {
        console.log("Error retrieving compromiso");
      } else {
        res.json(compromiso);
      }
    });
});


router.get('/compromiso/:id', function (req, res) {
  console.log('Get request for a single compromiso');
  compromiso.findById(req.params.id)
    .exec(function (err, compromiso) {
      if (err) {
        console.log("Error retrieving compromiso");
      } else {
        res.json(compromiso);
      }
    });
});


router.post('/compromiso', function (req, res) {
  console.log('Post compromiso');
  var newCompromiso = new Compromiso();
  newCompromiso.manualSinaes = req.body.manualSinaes;
  newCompromiso.programa = req.body.programa;
  newCompromiso.debilidad = req.body.debilidad;
  newCompromiso.causa = req.body.causa;

  newCompromiso.save(function (err, insertedCompromiso) {
    if (err) {
      console.log('Error al guardar compromiso');
    } else {
      res.json(insertedCompromiso);
    }
  });
});


router.put('/compromiso/:id', function (req, res) {
  console.log('Actualizar compromiso');
  Compromiso.findByIdAndUpdate(req.params.id,
    {
      $set: {
        manualSinaes: req.body.manualSinaes,
        programa: req.body.programa,
        debilidad: req.body.debilidad,
        causa: req.body.causa
      }
    },
    {
      new: true
    },
    function (err, updatedCompromiso) {
      if (err) {
        res.send("Error al actualizar compromiso");
      } else {
        res.json(updatedCompromiso);
      }
    }

  );
});

router.delete('/compromiso/:id', function (req, res) {
  console.log('Deleting a compromiso');
  Compromiso.findByIdAndRemove(req.params.id, function (err, deletedCompromiso) {
    if (err) {
      res.send("Error deleting compromiso");
    } else {
      res.json(deletedCompromiso);
    }
  });
});




module.exports = router; 
