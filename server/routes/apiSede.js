const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
var path = require('path');
const sede = require('../models/sede.model');



const db = "mongodb://sred@ds125489.mlab.com:25489/sred";
mongoose.Promise = global.Promise;

mongoose.connect(db, function(err){
    if(err){
        console.error("Error! " + err);
    }
});

router.get('/sedes', function(req, res){
    console.log('Get request for sede');
    sede.find({})
    .exec(function(err, sede){
        if (err){
            console.log("Error retrieving sede");
        }else {
            res.json(sede);
        }
    });
});


router.get('/sede/:id', function(req, res){
    console.log('Get request for a single sede');
    sede.findById(req.params.id)
    .exec(function(err, sde){
        if (err){
            console.log("Error retrieving sede");
        }else {
            res.json(sede);
        }
    });
});


router.post('/sede', function(req, res){
    console.log('Post sede');
    var newSede = new sede();
    newSede.nombre = req.body.nombre;
    

    newSede.save(function(err, insertedSede){
        if (err){
            console.log('Error al guardar sede');
        }else{
            res.json(insertedSede);
        }
    });
});

router.put('/sede/:id', function(req, res){
    console.log('Actualizar sede');
    sede.findByIdAndUpdate(req.params.id,
    {
        $set: {nombre: req.body.nombre}
    },
    {
        new: true
    },
    function(err, updatedSede){
        if(err){
            res.send("Error al actualizar sede");
        }else{
            res.json(updatedSede);
        }
    }

    );
});

router.delete('/sede/:id', function(req, res){
    console.log('Deleting a sede');
    sede.findByIdAndRemove(req.params.id, function(err, deletedSede){
        if(err){
            res.send("Error deleting sede");
        }else{
            res.json(deletedSede);
        }
    });
});




module.exports = router; 
