const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
var path = require('path');
const usuario = require('../models/usuario.model');



const db = "mongodb://sred@ds125489.mlab.com:25489/sred";
mongoose.Promise = global.Promise;

mongoose.connect(db, function(err){
    if(err){
        console.error("Error! " + err);
    }
});

router.get('/usuarios', function(req, res){
    console.log('Get request for usuario');
    usuario.find({})
    .exec(function(err, usuario){
        if (err){
            console.log("Error retrieving usuario");
        }else {
            res.json(usuario);
        }
    });
});


router.get('/usuario/:id', function(req, res){
    console.log('Get request for a single usuario');
    usuario.findById(req.params.id)
    .exec(function(err, usuario){
        if (err){
            console.log("Error retrieving usuario");
        }else {
            res.json(usuario);
        }
    });
});


router.post('/usuario', function(req, res){
    console.log('Post usuario');
    var newUsuario = new usuario();
    newUsuario.nombre = req.body.nombre;
    newUsuario.apellidos = req.body.apellidos;
    newUsuario.puesto = req.body.puesto;
    newUsuario.sede = req.body.sede;
    newUsuario.rol = req.body.rol;
    newUsuario.email = req.body.email;
    newUsuario.telefono = req.body.telefono;
    newUsuario.password = req.body.password;
    

    newUsuario.save(function(err, insertedUsuario){
        if (err){
            console.log('Error al guardar usuario');
        }else{
            res.json(insertedUsuario);
        }
    });
});

router.put('/usuario/:id', function(req, res){
    console.log('Actualizar usuario');
    usuario.findByIdAndUpdate(req.params.id,
    {
        $set: {nombre: req.body.nombre, 
            apellidos: req.body.apellidos, 
            puesto: req.body.puesto,
            nombreUsuario : req.body.nombreUsuario,
            rol: req.body.rol, 
            email:req.body.email,
            sede:req.body.sede}
    },
    {
        new: true
    },
    function(err, updatedUsuario){
        if(err){
            res.send("Error al actualizar usuario");
        }else{
            res.json(updatedUsuario);
        }
    }

    );
});

router.delete('/usuario/:id', function(req, res){
    console.log('Deleting a usuario');
    usuario.findByIdAndRemove(req.params.id, function(err, deletedUsuario){
        if(err){
            res.send("Error deleting usuario");
        }else{
            res.json(deletedUsuario);
        }
    });
});




module.exports = router; 
