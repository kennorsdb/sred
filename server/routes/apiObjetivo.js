const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
var path = require('path');
const Objetivo = require('../models/objetivo.model');



const db = "mongodb://sred@ds125489.mlab.com:25489/sred";
mongoose.Promise = global.Promise;

mongoose.connect(db, function(err){
    if(err){
        console.error("Error! " + err);
    }
});

router.get('/objetivo', function(req, res){
    console.log('Get request for objetivo');
    Objetivo.find({})
    .populate('compromisoMejora', 'manualSinaes')
    .exec(function(err, objetivo){
    	
        if (err){
            console.log("Error retrieving objetivo");
        }else {
            res.json(objetivo);
        }
    });
});

router.get('/objetivoByCompromiso/:id', function (req, res) {
    console.log('Get request for compromiso');
    Objetivo.find({ "compromisoMejora": req.params.id } )
    .populate('compromisoMejora')
      .exec(function (err, compromiso) {
        if (err) {
          console.log("Error retrieving compromiso");
        } else {
          res.json(compromiso);
        }
      });
  });

router.get('/objetivo/:id', function(req, res){
    console.log('Get request for a single objetivo');
    Objetivo.findById(req.params.id)
    .populate('compromisoMejora')
    .exec(function(err, objetivo){
        if (err){
            console.log("Error retrieving objetivo");
        }else {
            res.json(objetivo);
        }
    });
});


router.post('/objetivo', function(req, res){
    console.log('Post objetivo');
    var newObjetivo = new Objetivo();
    newObjetivo.compromisoMejora = req.body.compromisoMejora;
    newObjetivo.indicadores = req.body.indicadores;
    newObjetivo.objetivo = req.body.objetivo;



    newObjetivo.save(function(err, insertedObjetivo){
        if (err){
            console.log('Error al guardar objetivo');
        }else{
            res.json(insertedObjetivo);
        }
    });
});

router.put('/objetivo/:id', function(req, res){
    console.log('Actualizar objetivo');
    Objetivo.findByIdAndUpdate(req.params.id,
    {
        $set: {compromisoMejora: req.body.compromisoMejora, 
            indicadores: req.body.indicadores,
            objetivo: req.body.objetivo
        }
    },
    {
        new: true
    },
    function(err, updatedObejtivo){
        if(err){
            res.send("Error al actualizar objetivo");
        }else{
            res.json(updatedObejtivo);
        }
    }

    );
});

router.delete('/objetivo/:id', function(req, res){
    console.log('Deleting a objetivo');
    Objetivo.findByIdAndRemove(req.params.id, function(err, deletedObjetivo){
        if(err){
            res.send("Error deleting objetivo");
        }else{
            res.json(deletedObjetivo);
        }
    });
});




module.exports = router; 
